﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using Senior_Capstone_2;
using Senior_Capstone_2.Models;
using Microsoft.AspNet.Identity;

namespace Senior_Capstone_2.Controllers
{
    public class RostersController : CustomBaseController
    {
        private ESL_DB_Model db = new ESL_DB_Model();

        // GET: Rosters
        [Authorize(Roles = "Admin")]
        public ActionResult Index()
        {
            return View(db.Roster.ToList());
        }



        public JsonResult Check(int shelterid)
        {

            var cid = User.Identity.GetUserId();
            int temp1 = 0;
            int temp2 = 0;
            int temp3 = 0;
            bool temp4 = false;

            var getall = from a in db.Roster
                         where a.ShelterId == shelterid
                         select a;


            foreach (var user in getall)
            {
                var getdependants = from d in db.Dependants
                                    where d.SponsorId == user.UserId
                                    select d;
                temp1 += getdependants.Count();
                temp1++;
            }

            var sheltercap = from b in db.Shelters
                             where b.ShelterId == shelterid
                             select b;

            foreach (var x in sheltercap)
            {
                temp2 = x.MaxCapacity;
            }

            var dependantquery = from c in db.Dependants
                                 where c.SponsorId == cid
                                 select c;
            temp3 = dependantquery.Count();
            temp3++;

            var isCheckedIn = from f in db.Roster
                              where f.UserId == cid
                              select f;

            if (isCheckedIn.Count() >= 1)   //checkin status
            {
                temp4 = true;
            }

            object model123 = new[] {
                    new {occupancy = temp1, capacity = temp2, usersize = temp3, checkedin = temp4 }
            };

            return Json(model123, JsonRequestBehavior.AllowGet);
        }
        public JsonResult Check1()
        {

            var cid = User.Identity.GetUserId();
            bool temp4 = false;
            

            var isCheckedIn = from f in db.Roster
                              where f.UserId == cid
                              select f;

            if (isCheckedIn.Count() >= 1)   //checkin status
            {
                temp4 = true;
            }

            object model123 = new[] {
                    new {checkedin = temp4 }
            };

            return Json(model123, JsonRequestBehavior.AllowGet);
        }

        public JsonResult CheckIn(int shelterid)
        {
            var cid = User.Identity.GetUserId();
            int temp1 = 0;
            int temp2 = 0;
            int temp3 = 0;
            bool bool1 = false;
            bool bool2 = false;
            bool bool3 = true;

            var getall = from a in db.Roster
                         where a.ShelterId == shelterid
                         select a;

            foreach (var user in getall)
            {
                var getdependants = from d in db.Dependants
                                    where d.SponsorId == user.UserId
                                    select d;
                temp1 += getdependants.Count();
                temp1++;
            }

            var sheltercap = from b in db.Shelters
                             where b.ShelterId == shelterid
                             select b;

            var isCheckedIn = from f in db.Roster
                              where f.UserId == cid
                              select f;

            foreach (var x in sheltercap)
            {
                temp2 = x.MaxCapacity;
            }

            var dependantquery = from c in db.Dependants
                                 where c.SponsorId == cid
                                 select c;

            temp3 = dependantquery.Count();
            temp3++;


            //login status
            if (cid != null)
            {
                bool2 = true;
                if (isCheckedIn.Count() >= 1)   //checkin status
                {
                    bool3 = true;
                    //already checked in
                    object checkinResponse1 = new[] {
                    new {isempty = bool1, islogin = bool2 , ischeckin = bool3}
            };
                    return Json(checkinResponse1, JsonRequestBehavior.AllowGet);
                }

                if (!(temp1 + temp3 > temp2))
                {
                    Roster roster1 = new Roster();
                    roster1.ShelterId = shelterid;
                    roster1.UserId = cid;
                    db.Roster.Add(roster1);
                    db.SaveChanges();
                    //has space
                    bool1 = true;
                    bool3 = false;
                    object checkinResponse1 = new[] {
                    new {isempty = bool1, islogin = bool2 , ischeckin = bool3}
            };
                    return Json(checkinResponse1, JsonRequestBehavior.AllowGet);
                }
                else {
                    //over capacity
                    bool3 = false;
                    object checkinResponse2 = new[] {
                    new {isempty = bool1, islogin = bool2 , ischeckin = bool3}
            };
                    return Json(checkinResponse2, JsonRequestBehavior.AllowGet);
                }
            }
            //not logged in
            bool2 = false;
            object checkinResponse = new[] {
                    new {isempty = bool1, islogin = bool2 ,ischeckin = bool3}
            };
            return Json(checkinResponse, JsonRequestBehavior.AllowGet);
        }



        //check out functionality 

        public JsonResult CheckOut()
        {
            var confirmation = false;
            var cid = User.Identity.GetUserId();

            var deluserfrmshelt = from a in db.Roster
                                  where a.UserId == cid
                                  select a;
            foreach (var x in deluserfrmshelt)
            {
                db.Roster.Remove(x);
                confirmation = true;
            }

            db.SaveChanges();
            return Json(confirmation, JsonRequestBehavior.AllowGet);

        }


        [Authorize(Roles = "Admin")]
        // GET: Rosters/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Roster shelterRoster = db.Roster.Find(id);
            if (shelterRoster == null)
            {
                return HttpNotFound();
            }
            return View(shelterRoster);
        }

        // GET: Rosters/Create
        [Authorize(Roles = "Admin")]
        public ActionResult Create()
        {
            ViewBag.UserId = new SelectList(db.AspNetUsers, "Id", "Email");
            ViewBag.ShelterId = new SelectList(db.Shelters, "ShelterId", "ShelterName");
            return View();
        }

        // POST: Rosters/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "ShelterId,UserId,IsCheckedIn")] Roster shelterRoster)
        {
            if (ModelState.IsValid)
            {
                db.Roster.Add(shelterRoster);
                db.SaveChanges();
                return RedirectToAction("Index", "Admin");
            }

            ViewBag.UserId = new SelectList(db.AspNetUsers, "Id", "Email", shelterRoster.UserId);
            ViewBag.ShelterId = new SelectList(db.Shelters, "ShelterId", "ShelterName", shelterRoster.ShelterId);
            return View(shelterRoster);
        }

        [Authorize(Roles = "Admin")]
        // GET: Rosters/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Roster shelterRoster = db.Roster.Find(id);
            if (shelterRoster == null)
            {
                return HttpNotFound();
            }
            ViewBag.UserId = new SelectList(db.AspNetUsers, "Id", "Email", shelterRoster.UserId);
            ViewBag.ShelterId = new SelectList(db.Shelters, "ShelterId", "ShelterName", shelterRoster.ShelterId);
            return View(shelterRoster);
        }

        // POST: Rosters/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "ShelterId,UserId,IsCheckedIn")] Roster shelterRoster)
        {
            if (ModelState.IsValid)
            {
                db.Entry(shelterRoster).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index", "Admin");
            }
            ViewBag.UserId = new SelectList(db.AspNetUsers, "Id", "Email", shelterRoster.UserId);
            ViewBag.ShelterId = new SelectList(db.Shelters, "ShelterId", "ShelterName", shelterRoster.ShelterId);
            return View(shelterRoster);
        }

        // GET: Rosters/Delete/5
        public ActionResult Delete(string id)
        {
            if (id == "")
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            var rosterResult = from x in db.Roster
                               where x.UserId == id
                               select x;
            foreach (var x in rosterResult)
            {
                db.Roster.Remove(x);
            }
            db.SaveChanges();
            return RedirectToAction("Index", "Admin");
        }

        // POST: Rosters/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(string id)
        {
            var rosterResult = from x in db.Roster
                               where x.UserId == id
                               select x;
            foreach (var x in rosterResult)
            {
                db.Roster.Remove(x);
            }
            db.SaveChanges();
            return RedirectToAction("Index", "Admin");
        }


        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}