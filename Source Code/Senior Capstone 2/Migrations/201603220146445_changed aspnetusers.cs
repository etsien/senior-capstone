namespace Senior_Capstone_2.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class changedaspnetusers : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.AspNetRoles",
                c => new
                    {
                        Id = c.String(nullable: false, maxLength: 128),
                        Name = c.String(nullable: false, maxLength: 256),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.AspNetUsers",
                c => new
                    {
                        Id = c.String(nullable: false, maxLength: 128),
                        Email = c.String(maxLength: 256),
                        EmailConfirmed = c.Boolean(nullable: false),
                        PasswordHash = c.String(),
                        SecurityStamp = c.String(),
                        PhoneNumber = c.String(),
                        PhoneNumberConfirmed = c.Boolean(nullable: false),
                        TwoFactorEnabled = c.Boolean(nullable: false),
                        LockoutEndDateUtc = c.DateTime(),
                        LockoutEnabled = c.Boolean(nullable: false),
                        AccessFailedCount = c.Int(nullable: false),
                        UserName = c.String(nullable: false, maxLength: 256),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.AspNetUserClaims",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        UserId = c.String(nullable: false, maxLength: 128),
                        ClaimType = c.String(),
                        ClaimValue = c.String(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.AspNetUsers", t => t.UserId, cascadeDelete: true)
                .Index(t => t.UserId);
            
            CreateTable(
                "dbo.AspNetUserLogins",
                c => new
                    {
                        LoginProvider = c.String(nullable: false, maxLength: 128),
                        ProviderKey = c.String(nullable: false, maxLength: 128),
                        UserId = c.String(nullable: false, maxLength: 128),
                    })
                .PrimaryKey(t => new { t.LoginProvider, t.ProviderKey, t.UserId })
                .ForeignKey("dbo.AspNetUsers", t => t.UserId, cascadeDelete: true)
                .Index(t => t.UserId);
            
            CreateTable(
                "dbo.Users",
                c => new
                    {
                        UserID = c.Int(nullable: false, identity: true),
                        FirstName = c.String(nullable: false, maxLength: 128),
                        LastName = c.String(nullable: false, maxLength: 128),
                        DateOfBirth = c.DateTime(nullable: false, storeType: "date"),
                        PhoneNumber = c.String(maxLength: 50),
                        Address = c.String(maxLength: 128),
                        City = c.String(maxLength: 128),
                        ZipCode = c.Int(),
                        AspId = c.String(nullable: false, maxLength: 128),
                        IsAdmin = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => t.UserID)
                .ForeignKey("dbo.AspNetUsers", t => t.AspId, cascadeDelete: true)
                .Index(t => t.AspId);
            
            CreateTable(
                "dbo.Dependants",
                c => new
                    {
                        DependantId = c.Int(nullable: false, identity: true),
                        FirstName = c.String(nullable: false, maxLength: 128),
                        LastName = c.String(nullable: false, maxLength: 128),
                        DateOfBirth = c.DateTime(nullable: false, storeType: "date"),
                        SponsorId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.DependantId)
                .ForeignKey("dbo.Users", t => t.SponsorId, cascadeDelete: true)
                .Index(t => t.SponsorId);
            
            CreateTable(
                "dbo.ShelterRoster",
                c => new
                    {
                        ShelterId = c.Int(nullable: false),
                        UserId = c.Int(nullable: false),
                        IsCheckedIn = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => new { t.ShelterId, t.UserId })
                .ForeignKey("dbo.Shelters", t => t.ShelterId, cascadeDelete: true)
                .ForeignKey("dbo.Users", t => t.UserId, cascadeDelete: true)
                .Index(t => t.ShelterId)
                .Index(t => t.UserId);
            
            CreateTable(
                "dbo.Shelters",
                c => new
                    {
                        ShelterId = c.Int(nullable: false),
                        ShelterName = c.String(nullable: false, maxLength: 128),
                        PhoneNumber = c.String(nullable: false, maxLength: 50),
                        Address = c.String(nullable: false, maxLength: 128),
                        City = c.String(nullable: false, maxLength: 128),
                        ZipCode = c.String(nullable: false, maxLength: 5),
                    })
                .PrimaryKey(t => t.ShelterId);
            
            CreateTable(
                "dbo.Criteria",
                c => new
                    {
                        Criteria = c.String(nullable: false, maxLength: 128),
                        ShelterId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => new { t.Criteria, t.ShelterId })
                .ForeignKey("dbo.Shelters", t => t.ShelterId, cascadeDelete: true)
                .Index(t => t.ShelterId);
            
            CreateTable(
                "dbo.__MigrationHistory",
                c => new
                    {
                        MigrationId = c.String(nullable: false, maxLength: 150),
                        ContextKey = c.String(nullable: false, maxLength: 300),
                        Model = c.Binary(nullable: false),
                        ProductVersion = c.String(nullable: false, maxLength: 32),
                    })
                .PrimaryKey(t => new { t.MigrationId, t.ContextKey });
            
            CreateTable(
                "dbo.AspNetUserRoles",
                c => new
                    {
                        RoleId = c.String(nullable: false, maxLength: 128),
                        UserId = c.String(nullable: false, maxLength: 128),
                    })
                .PrimaryKey(t => new { t.RoleId, t.UserId })
                .ForeignKey("dbo.AspNetRoles", t => t.RoleId, cascadeDelete: true)
                .ForeignKey("dbo.AspNetUsers", t => t.UserId, cascadeDelete: true)
                .Index(t => t.RoleId)
                .Index(t => t.UserId);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.AspNetUserRoles", "UserId", "dbo.AspNetUsers");
            DropForeignKey("dbo.AspNetUserRoles", "RoleId", "dbo.AspNetRoles");
            DropForeignKey("dbo.Users", "AspId", "dbo.AspNetUsers");
            DropForeignKey("dbo.ShelterRoster", "UserId", "dbo.Users");
            DropForeignKey("dbo.ShelterRoster", "ShelterId", "dbo.Shelters");
            DropForeignKey("dbo.Criteria", "ShelterId", "dbo.Shelters");
            DropForeignKey("dbo.Dependants", "SponsorId", "dbo.Users");
            DropForeignKey("dbo.AspNetUserLogins", "UserId", "dbo.AspNetUsers");
            DropForeignKey("dbo.AspNetUserClaims", "UserId", "dbo.AspNetUsers");
            DropIndex("dbo.AspNetUserRoles", new[] { "UserId" });
            DropIndex("dbo.AspNetUserRoles", new[] { "RoleId" });
            DropIndex("dbo.Criteria", new[] { "ShelterId" });
            DropIndex("dbo.ShelterRoster", new[] { "UserId" });
            DropIndex("dbo.ShelterRoster", new[] { "ShelterId" });
            DropIndex("dbo.Dependants", new[] { "SponsorId" });
            DropIndex("dbo.Users", new[] { "AspId" });
            DropIndex("dbo.AspNetUserLogins", new[] { "UserId" });
            DropIndex("dbo.AspNetUserClaims", new[] { "UserId" });
            DropTable("dbo.AspNetUserRoles");
            DropTable("dbo.__MigrationHistory");
            DropTable("dbo.Criteria");
            DropTable("dbo.Shelters");
            DropTable("dbo.ShelterRoster");
            DropTable("dbo.Dependants");
            DropTable("dbo.Users");
            DropTable("dbo.AspNetUserLogins");
            DropTable("dbo.AspNetUserClaims");
            DropTable("dbo.AspNetUsers");
            DropTable("dbo.AspNetRoles");
        }
    }
}
