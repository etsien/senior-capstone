﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(Senior_Capstone_2.Startup))]
namespace Senior_Capstone_2
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
