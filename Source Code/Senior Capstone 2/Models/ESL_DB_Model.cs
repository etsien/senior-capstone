namespace Senior_Capstone_2
{
    using System;
    using System.Data.Entity;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Linq;

    public partial class ESL_DB_Model : DbContext
    {
        public ESL_DB_Model()
            : base("name=ESL_DB_Model")
        {
        }

        public virtual DbSet<C__MigrationHistory> C__MigrationHistory { get; set; }
        public virtual DbSet<AspNetRoles> AspNetRoles { get; set; }
        public virtual DbSet<AspNetUserClaims> AspNetUserClaims { get; set; }
        public virtual DbSet<AspNetUserLogins> AspNetUserLogins { get; set; }
        public virtual DbSet<AspNetUsers> AspNetUsers { get; set; }
        public virtual DbSet<Criteria> Criteria { get; set; }
        public virtual DbSet<SpecialNeeds> SpecialNeeds { get; set; }
        public virtual DbSet<Dependants> Dependants { get; set; }
        public virtual DbSet<Roster> Roster { get; set; }
        public virtual DbSet<Shelters> Shelters { get; set; }
        public virtual DbSet<ContactAdmins> ContactAdmins { get; set; }
        public virtual DbSet<TicketTypes> TicketTypes { get; set; }
        //public virtual DbSet<Users> Users { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Entity<AspNetRoles>()
                .HasMany(e => e.AspNetUsers)
                .WithMany(e => e.AspNetRoles)
                .Map(m => m.ToTable("AspNetUserRoles").MapLeftKey("RoleId").MapRightKey("UserId"));

            modelBuilder.Entity<AspNetUsers>()
                .HasMany(e => e.AspNetUserClaims)
                .WithRequired(e => e.AspNetUsers)
                .HasForeignKey(e => e.UserId);

            modelBuilder.Entity<AspNetUsers>()
                .HasMany(e => e.AspNetUserLogins)
                .WithRequired(e => e.AspNetUsers)
                .HasForeignKey(e => e.UserId);

            //modelBuilder.Entity<AspNetUsers>()
            //    .HasMany(e => e.Users)
            //    .WithRequired(e => e.AspNetUsers)
            //    .HasForeignKey(e => e.AspId);

            modelBuilder.Entity<AspNetUsers>()
                .HasMany(e => e.Dependants)
                .WithRequired(e => e.AspNetUsers)
                .HasForeignKey(e => e.SponsorId);
        }

        //public System.Data.Entity.DbSet<Senior_Capstone_2.ContactAdmin> ContactAdmins { get; set; }
    }
}
