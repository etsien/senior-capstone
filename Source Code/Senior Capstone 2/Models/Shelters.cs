namespace Senior_Capstone_2
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class Shelters
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public Shelters()
        {
            Criteria = new HashSet<Criteria>();
            //Roster = new HashSet<Roster>();
        }

        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int ShelterId { get; set; }

        [Required]
        [Display(Name = "Shelter Name")]
        [StringLength(128)]
        public string ShelterName { get; set; }

        [Required(ErrorMessage = "You must provide a Phone Number")]
        [DataType(DataType.PhoneNumber)]
        [StringLength(50)]
        [RegularExpression(@"^\(?([0-9]{3})\)?[-. ]?([0-9]{3})[-. ]?([0-9]{4})$", ErrorMessage = "Not a valid Phone number")]
        [Display(Name = "Phone Number")]
        public string PhoneNumber { get; set; }

        [Required]
        [StringLength(128)]
        public string Address { get; set; }

        [Required]
        [StringLength(128)]
        public string City { get; set; }

        [Required]
        [StringLength(5)]
        [Display(Name = "Zip Code")]        
        public string ZipCode { get; set; }

        [Required]
        [Display(Name = "Max Capacity")]
        public int MaxCapacity { get; set; }


        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Criteria> Criteria { get; set; }

        
    }
}
