﻿namespace Senior_Capstone_2
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class ContactAdmins
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int TicketId { get; set; }

        [EmailAddress]
        public string Email { get; set; }

        [StringLength(256)]
        public string Subject { get; set; }

        public string Body { get; set; }
        
        [Display(Name = "Ticket Type")]
        public int TypeId { get; set; }

        [Display(Name = "Is Resolved")]
        public bool IsResolved { get; set; }

        public virtual TicketTypes TicketTypes { get; set; }
    }
}