namespace Senior_Capstone_2
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class Dependants
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int DependantId { get; set; }

        [Required]
        [StringLength(128)]
        [Display(Name = "First Name")]
        public string FirstName { get; set; }

        [Required]
        [StringLength(128)]
        [Display(Name = "Last Name")]
        public string LastName { get; set; }

        [DataType(DataType.Date)]
        [Display(Name = "Birth Date")]
        public DateTime DateOfBirth { get; set; }

        [StringLength(128)]
        public string SponsorId { get; set; }

        [ForeignKey("SponsorId")]
        public virtual AspNetUsers AspNetUsers { get; set; }
    }
}
