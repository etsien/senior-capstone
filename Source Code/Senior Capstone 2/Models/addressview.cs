namespace Senior_Capstone_2.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("addressview")]
    public partial class addressview
    {
        [Key]
        [Column(Order = 0)]
        public string Address { get; set; }

        [Key]
        [Column(Order = 1)]
        public string City { get; set; }

        [Key]
        [Column(Order = 2)]
        [StringLength(5)]
        public string ZipCode { get; set; }
    }
}
