// <auto-generated />
namespace Senior_Capstone_2.Migrations
{
    using System.CodeDom.Compiler;
    using System.Data.Entity.Migrations;
    using System.Data.Entity.Migrations.Infrastructure;
    using System.Resources;
    
    [GeneratedCode("EntityFramework.Migrations", "6.1.3-40302")]
    public sealed partial class _3212016_4 : IMigrationMetadata
    {
        private readonly ResourceManager Resources = new ResourceManager(typeof(_3212016_4));
        
        string IMigrationMetadata.Id
        {
            get { return "201603212016143_3-21-2016_4"; }
        }
        
        string IMigrationMetadata.Source
        {
            get { return null; }
        }
        
        string IMigrationMetadata.Target
        {
            get { return Resources.GetString("Target"); }
        }
    }
}
