﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using Senior_Capstone_2.Models;

namespace Senior_Capstone_2.Controllers
{
    public class SheltersController : CustomBaseController
    {
        private ESL_DB_Model db = new ESL_DB_Model();

        // GET: Shelters
        [Authorize(Roles = "Admin")]
        public ActionResult Index()
        {
            return View(db.Shelters.ToList());
        }

        [Authorize(Roles ="Admin")]
        // GET: Shelters/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Shelters shelters = db.Shelters.Find(id);
            if (shelters == null)
            {
                return HttpNotFound();
            }
            return View(shelters);
        }

        [Authorize(Roles = "Admin")]
        public ActionResult RosterList(int? id)
        {
            List<RosterDependantCount> temp1 = new List<RosterDependantCount>();

            var getusers = from a in db.Roster
                           where a.ShelterId == id
                           select a;

            foreach (var x in getusers)
            {
                RosterDependantCount temp2 = new RosterDependantCount();
                var getdependants = from b in db.Dependants
                                    where b.SponsorId == x.AspNetUsers.Id
                                    select b;
                temp2.UserRoster = x;
                temp2.DepCount = getdependants.Count();
                temp1.Add(temp2);
            }

            var shelterquery = from c in db.Shelters
                               where c.ShelterId == id
                               select c.ShelterName;
            foreach (var shelter in shelterquery)
            {
                ViewBag.ShelterName = shelter;
            }

            return View(temp1);
        }


        // GET: Shelters/Create
        [Authorize(Roles = "Admin")]
        public ActionResult Create()
        {
            return View();
        }

        // POST: Shelters/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "ShelterId,ShelterName,PhoneNumber,Address,City,ZipCode,MaxCapacity")] Shelters shelters)
        {
            if (ModelState.IsValid)
            {
                db.Shelters.Add(shelters);
                db.SaveChanges();
                return RedirectToAction("Index", "Admin");
            }

            return View(shelters);
        }

        [Authorize(Roles = "Admin")]
        // GET: Shelters/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Shelters shelters = db.Shelters.Find(id);
            if (shelters == null)
            {
                return HttpNotFound();
            }
            return View(shelters);
        }

        // POST: Shelters/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "ShelterId,ShelterName,PhoneNumber,Address,City,ZipCode,MaxCapacity")] Shelters shelters)
        {
            if (ModelState.IsValid)
            {
                db.Entry(shelters).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index", "Admin");
            }
            return View(shelters);
        }

        [Authorize(Roles = "Admin")]
        // GET: Shelters/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Shelters shelters = db.Shelters.Find(id);
            if (shelters == null)
            {
                return HttpNotFound();
            }
            return View(shelters);
        }

        // POST: Shelters/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Shelters shelters = db.Shelters.Find(id);
            db.Shelters.Remove(shelters);
            db.SaveChanges();
            return RedirectToAction("Index", "Admin");
        }

        
        public JsonResult getAddress()
        {
            var address = db.Shelters
                .OrderBy(x => x.ShelterId);
            return Json(address, JsonRequestBehavior.AllowGet);
        }

        public JsonResult getAddressSpecial(bool handicap, bool food, bool children, bool pets)
        {
            if (handicap || food || children || pets)    //if someone accidentally clicks special needs but selects nothing
            {
                List<SpecialNeeds> criteria = db.SpecialNeeds.Include("Shelters").ToList();
                if (handicap)
                {
                    criteria = criteria.Where(c => c.Handicap == handicap).ToList();
                }
                if (food)
                {
                    criteria = criteria.Where(c => c.Allergy == food).ToList();
                }
                if (children)
                {
                    criteria = criteria.Where(c => c.ChildCare == children).ToList();
                }
                if (pets)
                {
                    criteria = criteria.Where(c => c.PetFriendly == pets).ToList();
                }

                List<Shelters> shelters = new List<Shelters>();

                foreach (var criterion in criteria)
                {
                    shelters.Add(criterion.Shelters);
                }

                return Json(shelters, JsonRequestBehavior.AllowGet);
            }
            else
            {
                var address = db.Shelters
                .OrderBy(x => x.ShelterId);
                return Json(address, JsonRequestBehavior.AllowGet);
            }
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
