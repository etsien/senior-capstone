﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using Senior_Capstone_2;

namespace Senior_Capstone_2.Controllers
{
    public class CriteriaController : Controller
    {
        private ESL_DB_Model db = new ESL_DB_Model();

        // GET: Criteria
        public ActionResult Index()
        {
            var criteria = db.Criteria.Include(c => c.Shelters);
            return View(criteria.ToList());
        }

        // GET: Criteria/Details/5
        public ActionResult Details(string id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Criteria criteria = db.Criteria.Find(id);
            if (criteria == null)
            {
                return HttpNotFound();
            }
            return View(criteria);
        }

        // GET: Criteria/Create
        public ActionResult Create()
        {
            ViewBag.ShelterId = new SelectList(db.Shelters, "ShelterId", "ShelterName");
            return View();
        }

        // POST: Criteria/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "CriteriaName,ShelterId")] Criteria criteria)
        {
            if (ModelState.IsValid)
            {
                db.Criteria.Add(criteria);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.ShelterId = new SelectList(db.Shelters, "ShelterId", "ShelterName", criteria.ShelterId);
            return View(criteria);
        }

        // GET: Criteria/Edit/5
        public ActionResult Edit(string id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Criteria criteria = db.Criteria.Find(id);
            if (criteria == null)
            {
                return HttpNotFound();
            }
            ViewBag.ShelterId = new SelectList(db.Shelters, "ShelterId", "ShelterName", criteria.ShelterId);
            return View(criteria);
        }

        // POST: Criteria/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "CriteriaName,ShelterId")] Criteria criteria)
        {
            if (ModelState.IsValid)
            {
                db.Entry(criteria).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.ShelterId = new SelectList(db.Shelters, "ShelterId", "ShelterName", criteria.ShelterId);
            return View(criteria);
        }

        // GET: Criteria/Delete/5
        public ActionResult Delete(string id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Criteria criteria = db.Criteria.Find(id);
            if (criteria == null)
            {
                return HttpNotFound();
            }
            return View(criteria);
        }

        // POST: Criteria/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(string id)
        {
            Criteria criteria = db.Criteria.Find(id);
            db.Criteria.Remove(criteria);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
