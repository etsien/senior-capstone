﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.Mvc;

namespace Senior_Capstone_2.Controllers
{
    [Authorize(Roles = "Admin")]
    public class AdminController : CustomBaseController
    {
        // GET: Admin
        public ActionResult Index()
        {
            return View();
        }
    }
}