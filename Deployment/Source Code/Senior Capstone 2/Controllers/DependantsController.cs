﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using Microsoft.AspNet.Identity;
using System.Web.Security;
using System.Web.Mvc;
using Senior_Capstone_2;

namespace Senior_Capstone_2.Controllers
{
    public class DependantsController : CustomBaseController
    {
        private ESL_DB_Model db = new ESL_DB_Model();

        //// GET: Dependants
        [Authorize(Roles = "Admin")]
        public ActionResult Index()
        {
            var dependants = db.Dependants.Include(d => d.AspNetUsers);
            return View(dependants.ToList());
        }

        public ActionResult DependantList()
        {
            var cid = User.Identity.GetUserId();
            var dependants = from a in db.Dependants
                             where a.SponsorId == cid
                             select a;
            return View(dependants.ToList());
        }
        //var cid = User.Identity.GetUserId();
        //var dependants = from a in db.Dependants
        //                 where a.SponsorId == cid
        //                 select a;
        //    return View(dependants.ToList());


        // GET: Dependants/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Dependants dependants = db.Dependants.Find(id);
            if (dependants == null)
            {
                return HttpNotFound();
            }
            return View(dependants);
        }

        // GET: Dependants/Create
        public ActionResult Create()
        {
            var cid = User.Identity.GetUserId()/*Membership.GetUser().ProviderUserKey.ToString()*/;
            var currentuser = db.AspNetUsers
                .Where(x => x.Id == cid);

            foreach (var user in currentuser)
            {
                ViewBag.SponsorId = user.Id;
                return View();
            }
            return View();
        }

        // POST: Dependants/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "DependantId,FirstName,LastName,DateOfBirth,SponsorId")] Dependants dependants)
        {
            if (ModelState.IsValid)
            {
                db.Dependants.Add(dependants);
                db.SaveChanges();
                return RedirectToAction("DependantList", "Dependants");
            }

            ViewBag.SponsorId = new SelectList(db.AspNetUsers, "Id", "Email", dependants.SponsorId);
            return View(dependants);
        }

        // GET: Dependants/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Dependants dependants = db.Dependants.Find(id);
            if (dependants == null)
            {
                return HttpNotFound();
            }
            ViewBag.SponsorId = new SelectList(db.AspNetUsers, "Id", "Email", dependants.SponsorId);
            return View(dependants);
        }

        // POST: Dependants/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "DependantId,FirstName,LastName,DateOfBirth,SponsorId")] Dependants dependants)
        {
            if (ModelState.IsValid)
            {
                db.Entry(dependants).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("DependantList","Dependants");
            }
            ViewBag.SponsorId = new SelectList(db.AspNetUsers, "Id", "Email", dependants.SponsorId);
            return View(dependants);
        }

        // GET: Dependants/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Dependants dependants = db.Dependants.Find(id);
            if (dependants == null)
            {
                return HttpNotFound();
            }
            return View(dependants);
        }

        // POST: Dependants/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Dependants dependants = db.Dependants.Find(id);
            db.Dependants.Remove(dependants);
            db.SaveChanges();
            return RedirectToAction("DependantList","Dependants");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
