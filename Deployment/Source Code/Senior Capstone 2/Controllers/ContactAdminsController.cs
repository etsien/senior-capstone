﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using Senior_Capstone_2;

namespace Senior_Capstone_2.Controllers
{
    public class ContactAdminsController : CustomBaseController
    {
        private ESL_DB_Model db = new ESL_DB_Model();

        // GET: ContactAdmins
        [Authorize(Roles = "Admin")]
        public ActionResult Index()
        {
            var contactAdmins = db.ContactAdmins.Include(c => c.TicketTypes);
            return View(contactAdmins.ToList());
        }

        // GET: ContactAdmins/Details/5
        [Authorize(Roles = "Admin")]
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            ContactAdmins contactAdmins = db.ContactAdmins.Find(id);
            if (contactAdmins == null)
            {
                return HttpNotFound();
            }
            return View(contactAdmins);
        }

        // GET: ContactAdmins/Create
        public ActionResult Create()
        {
            ViewBag.TypeId = new SelectList(db.TicketTypes, "TypeId", "TypeName");
            return View();
        }

        // POST: ContactAdmins/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "TicketId,Email,Subject,Body,TypeId,IsResolved")] ContactAdmins contactAdmins)
        {
            if (ModelState.IsValid)
            {
                db.ContactAdmins.Add(contactAdmins);
                db.SaveChanges();
                return RedirectToAction("Index","Home");
            }

            ViewBag.TypeId = new SelectList(db.TicketTypes, "TypeId", "TypeName", contactAdmins.TypeId);
            return View(contactAdmins);
        }


        // GET: ContactAdmins/Edit/5
        [Authorize(Roles = "Admin")]
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            ContactAdmins contactAdmins = db.ContactAdmins.Find(id);
            if (contactAdmins == null)
            {
                return HttpNotFound();
            }
            ViewBag.TypeId = new SelectList(db.TicketTypes, "TypeId", "TypeName", contactAdmins.TypeId);
            return View(contactAdmins);
        }

        // POST: ContactAdmins/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        [Authorize(Roles = "Admin")]
        public ActionResult Edit([Bind(Include = "TicketId,Email,Subject,Body,TypeId,IsResolved")] ContactAdmins contactAdmins)
        {
            if (ModelState.IsValid)
            {
                db.Entry(contactAdmins).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index","Admin");
            }
            ViewBag.TypeId = new SelectList(db.TicketTypes, "TypeId", "TypeName", contactAdmins.TypeId);
            return View(contactAdmins);
        }

        // GET: ContactAdmins/Delete/5
        [Authorize(Roles = "Admin")]
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            ContactAdmins contactAdmins = db.ContactAdmins.Find(id);
            if (contactAdmins == null)
            {
                return HttpNotFound();
            }
            return View(contactAdmins);
        }

        // POST: ContactAdmins/Delete/5
        [HttpPost, ActionName("Delete")]
        [Authorize(Roles = "Admin")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            ContactAdmins contactAdmins = db.ContactAdmins.Find(id);
            db.ContactAdmins.Remove(contactAdmins);
            db.SaveChanges();
            return RedirectToAction("Index", "Admin");
        }
        [Authorize(Roles = "Admin")]
        public ActionResult redirecttoindex() {
            return RedirectToAction("Index", "Home");
        }
        [Authorize(Roles = "Admin")]
        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
