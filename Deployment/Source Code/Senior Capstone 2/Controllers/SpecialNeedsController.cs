﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using Senior_Capstone_2;

namespace Senior_Capstone_2.Controllers
{
    [Authorize(Roles = "Admin")]
    public class SpecialNeedsController : CustomBaseController
    {
        private ESL_DB_Model db = new ESL_DB_Model();

        // GET: SpecialNeeds
        public ActionResult Index()
        {
            var specialNeeds = db.SpecialNeeds.Include(s => s.Shelters);
            return View(specialNeeds.ToList());
        }

        // GET: SpecialNeeds/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            SpecialNeeds specialNeeds = db.SpecialNeeds.Find(id);
            if (specialNeeds == null)
            {
                return HttpNotFound();
            }
            return View(specialNeeds);
        }

        // GET: SpecialNeeds/Create
        public ActionResult Create()
        {
            ViewBag.ShelterId = new SelectList(db.Shelters, "ShelterId", "ShelterName");
            return View();
        }

        // POST: SpecialNeeds/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "ShelterId,Handicap,Allergy,ChildCare,PetFriendly")] SpecialNeeds specialNeeds)
        {
            if (ModelState.IsValid)
            {
                db.SpecialNeeds.Add(specialNeeds);
                db.SaveChanges();
                return RedirectToAction("Index", "Admin");
            }

            ViewBag.ShelterId = new SelectList(db.Shelters, "ShelterId", "ShelterName", specialNeeds.ShelterId);
            return View(specialNeeds);
        }

        // GET: SpecialNeeds/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            SpecialNeeds specialNeeds = db.SpecialNeeds.Find(id);
            if (specialNeeds == null)
            {
                return HttpNotFound();
            }
            ViewBag.ShelterId = new SelectList(db.Shelters, "ShelterId", "ShelterName", specialNeeds.ShelterId);
            return View(specialNeeds);
        }

        // POST: SpecialNeeds/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "ShelterId,Handicap,Allergy,ChildCare,PetFriendly")] SpecialNeeds specialNeeds)
        {
            if (ModelState.IsValid)
            {
                db.Entry(specialNeeds).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index", "Admin");
            }
            ViewBag.ShelterId = new SelectList(db.Shelters, "ShelterId", "ShelterName", specialNeeds.ShelterId);
            return View(specialNeeds);
        }

        // GET: SpecialNeeds/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            SpecialNeeds specialNeeds = db.SpecialNeeds.Find(id);
            if (specialNeeds == null)
            {
                return HttpNotFound();
            }
            return View(specialNeeds);
        }

        // POST: SpecialNeeds/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            SpecialNeeds specialNeeds = db.SpecialNeeds.Find(id);
            db.SpecialNeeds.Remove(specialNeeds);
            db.SaveChanges();
            return RedirectToAction("Index", "Admin");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
