﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Senior_Capstone_2.Models
{
    public class RosterDependantCount
    {
        public Roster UserRoster { get; set; }

        [Display(Name = "Dependant Count")]
        public int DepCount { get; set; }

    }
}