namespace Senior_Capstone_2
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class Criteria
    {
        [Key]
        [Column("Criteria", Order = 0)]
        public string Criteria1 { get; set; }




        //[Required]
        //public bool Handicap { get; set; }

        //[Required]
        //public bool Allergy { get; set; }

        //[Required]
        //public bool ChildCare { get; set; }

        //[Required]
        //public bool PetFriendly { get; set; }


        [Key]
        [Column(Order = 1)]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int ShelterId { get; set; }

        public virtual Shelters Shelters { get; set; }
    }
}
