﻿namespace Senior_Capstone_2
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class SpecialNeeds
    {
        [Required]
        public bool Handicap { get; set; }

        [Required]
        public bool Allergy { get; set; }

        [Required]
        [Display(Name = "Child Care Services")]
        public bool ChildCare { get; set; }

        [Required]
        [Display(Name = "Pet Friendly")]
        public bool PetFriendly { get; set; }


        [Key]
        [ForeignKey("Shelters")]
        [Column(Order = 1)]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int ShelterId { get; set; }


        [Display(Name = "Shelter Name")]
        public virtual Shelters Shelters { get; set; }
    }
}
