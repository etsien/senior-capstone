# README #

Senior Capstone Project
Members: Edward Tsien, Alex Taylor, Lam Le

### TITLE: EMERGENCY SHELTER LOCATOR ###

* Aids users in locating emergency shelters allowing for several filtering criteria and displaying results on a map.

### How do I get set up? ###

* Requires Visual Studio 2015 and an internet connection
* Copy the Deployment folder to a local drive
* Open solution (located at ~/Source Code/Senior Capstone 2.sln)
* Build (allow NuGet to reacquire packages as necessary)
* and Run in Chrome or Firefox
